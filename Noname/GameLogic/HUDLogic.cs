﻿using System;
using Microsoft.Xna.Framework;
using Noname.GameObjects;

namespace Noname.GameLogic
{
    public class HUDLogic
    {
        /// <summary>
        /// Calculates the FrameCount. Relies on FPSCounter
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="fpsCounter"> </param>
        public void CalculateFPS(GameTime gameTime, FPSCounter fpsCounter)
        {
            // look if the last update is a second ago
            fpsCounter.ElapsedTime += gameTime.ElapsedGameTime;
            if (fpsCounter.ElapsedTime >= TimeSpan.FromSeconds(1))
            {
                // subtract 1 second, so we have accurate timers
                fpsCounter.ElapsedTime -= TimeSpan.FromSeconds(1);


                // FrameCount is set in the HUDRenderer
                fpsCounter.FPS = fpsCounter.FrameCount;


                // Reset the counter to 0
                fpsCounter.FrameCount = 0;
            }
            fpsCounter.Text = fpsCounter.FPS.ToString();
        }

    }
}
