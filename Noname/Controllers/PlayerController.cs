﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Noname.GameObjects;

namespace Noname.Controllers
{
    public class PlayerController
    {
        private readonly Player _player;

        private KeyboardState _keyboardState;

        public PlayerController(Player player)
        {
            _player = player;
        }

        public void Update(GameTime gameTime)
        {
            _keyboardState = Keyboard.GetState();

            if (_keyboardState.IsKeyDown(Keys.Right))
                Move(1, 0);
            else if (_keyboardState.IsKeyDown(Keys.Left))
                Move(-1, 0);
            else if (_keyboardState.IsKeyDown(Keys.Up))
                Move(0, -1);
            else if (_keyboardState.IsKeyDown(Keys.Down))
                Move(0, 1);
        }

        private void Move(int x, int y)
        {
            _player.Position += new Vector2(x, y);
        }
    }
}
