﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Noname.GameObjects;

namespace Noname.Controllers
{
    public class WorldController
    {
        private readonly World _world;

        private KeyboardState   _keyboardState;
        private MouseState      _mouseState;

        public WorldController(World world)
        {
            _world = world;
        }

        public void Update(GameTime gameTime)
        {
            _keyboardState  = Keyboard.GetState();
            _mouseState     = Mouse.GetState();
        }
    }
}
