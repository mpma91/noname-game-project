﻿using Microsoft.Xna.Framework;
using Noname.GameObjects;
using Noname.Helpers;

namespace Noname.Views
{
    internal class WorldRenderer : IRenderer
    {
        private readonly World _world;


        public WorldRenderer(World world)
        {
            _world = world;
        }


        #region IRenderer Members


        public void Render(GameTime gameTime, DrawContext drawContext)
        {
        }


        #endregion
    }

}
