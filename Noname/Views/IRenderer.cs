﻿using Microsoft.Xna.Framework;
using Noname.Helpers;

namespace Noname.Views
{
    internal interface IRenderer
    {
        void Render(GameTime gameTime, DrawContext drawContext);
    }

}
