﻿using Microsoft.Xna.Framework;
using Noname.GameObjects;
using Noname.Helpers;

namespace Noname.Views
{
    internal class HUDRenderer : IRenderer
    {
        private readonly World _world;


        public HUDRenderer(World world)
        {
            _world = world;
        }


        #region IRenderer Members


        public void Render(GameTime gameTime, DrawContext drawContext)
        {
            _world.FPSCounter.FrameCount += 1;

            /*
            if (_world.FPSCounter.Font != null)
            {
                drawContext.SpriteBatch.DrawString(_world.FPSCounter.Font, _world.FPSCounter.Text, _world.FPSCounter.Position, _world.FPSCounter.Color);
            }
            else
            {
                drawContext.SpriteBatch.DrawString(drawContext.DefaultFont, _world.FPSCounter.Text, _world.FPSCounter.Position, _world.FPSCounter.Color);
            }
            */
        }


        #endregion
    }

}
