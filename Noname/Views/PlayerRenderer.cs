﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Noname.GameObjects;
using Noname.Helpers;

namespace Noname.Views
{
    public class PlayerRenderer : IRenderer
    {
        private readonly World _world;


        public PlayerRenderer(World world)
        {
            _world = world;
        }


        #region IRenderer Members


        public void Render(GameTime gameTime, DrawContext drawContext)
        {
            drawContext.Draw(_world.Player.Sprite, _world.Player.Position);
        }


        #endregion
    }

}
