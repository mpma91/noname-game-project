﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Noname.Controllers;
using Noname.GameObjects;
using Noname.Helpers;
using Noname.Views;

namespace Noname.Engine
{
    internal class GameEngine
    {
        private readonly Game               _game;
        private readonly WorldController    _worldController;
        private readonly PlayerController   _playerController;
        private readonly World              _world;
        private readonly Player             _player;

        private readonly List<IRenderer>    _renderers = new List<IRenderer>();
        private readonly WorldRenderer      _worldRenderer;
        private readonly PlayerRenderer     _playerRenderer;
        private readonly HUDRenderer        _hudRenderer;

        public Texture2D PlayerSprite;
        public Vector2 Position = new Vector2(10, 10);

        public GameEngine(Game game)
        {
            _game               = game;
            _player = new Player(100, 100, 0.5f, Position, "player", _game.Content.Load<Texture2D>("SquareGuy"));
            _world              = new World(_player);


            // Controllers
            _worldController    = new WorldController(_world);
            _playerController   = new PlayerController(_player);

            // Views
            _worldRenderer      = new WorldRenderer(_world);
            _renderers.Add(_worldRenderer);

            _playerRenderer     = new PlayerRenderer(_world);
            _renderers.Add(_playerRenderer);

            _hudRenderer        = new HUDRenderer(_world);
            _renderers.Add(_hudRenderer);
        }

        public void Update(GameTime gameTime)
        {
            _worldController.Update(gameTime);
            _playerController.Update(gameTime);
        }

        public void Draw(GameTime gameTime, DrawContext drawContext)
        {
            drawContext.Begin();

            //drawContext.Draw(PlayerSprite, Position);

            // ADD DRAW METHODS
            foreach (var renderer in _renderers)
            {
                if (gameTime != null) renderer.Render(gameTime, drawContext);
            }
            // END DRAW METHODS

            drawContext.End();
        }

        /// <summary>
        /// Loads all the content
        /// </summary>
        /// <param name="content"></param>
        public void LoadContent(ContentManager content)
        {
            // PLAYER SPRITE
            PlayerSprite = content.Load<Texture2D>("SquareGuy");

            // ENEMY SPRITE
        }
    }
}
