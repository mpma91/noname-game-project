﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Noname.Helpers
{
    public class DrawContext
    {
        private readonly GraphicsDevice _graphicsDevice;


        public DrawContext(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice)
        {
            SpriteBatch = spriteBatch;
            _graphicsDevice = graphicsDevice;
        }


        public SpriteBatch SpriteBatch { get; set; }


        public SpriteFont DefaultFont { get; set; }




        public void Begin()
        {
            SpriteBatch.Begin();
        }


        public void End()
        {
            SpriteBatch.End();
        }

        public void Draw(Texture2D sprite, Vector2 position)
        {
            SpriteBatch.Draw(sprite, position);
        }
    }

}
