﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Noname.GameObjects
{
    /// <summary>
    /// All game actors will inherit from this class
    /// </summary>
    public abstract class SpriteEntity : Entity
    {
        protected SpriteEntity(int hp, int mp, float speed, Vector2 position, string name, Texture2D sprite)
        {
            HP          = hp;
            MP          = mp;
            Speed       = speed;
            Sprite      = sprite;
            Position    = position;
            EntityName  = name;
        }

        public Texture2D    Sprite  { get; set; }

        // hit points and mana points
        public int          HP      { get; set; }
        public int          MP      { get; set; }

        public float        Speed   { get; set; }
    }
}

