﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Noname.GameObjects
{

    public class Player : SpriteEntity
    {
        public Player(int hp, int mp, float speed, Vector2 position, string name, Texture2D sprite) 
            : base(hp, mp, speed, position, name, sprite)
        {
        }
    }

}
