﻿using System;
using Microsoft.Xna.Framework;

namespace Noname.GameObjects
{
    public abstract class Entity
    {
        public String   EntityName  { get; set; }
        public Vector2  Position    { get; set; }
    }
}

