﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Noname.GameObjects
{
    public abstract class TextEntity : Entity
    {
        public SpriteFont Font { get; set; }
        public Color Color { get; set; }
    }
}

