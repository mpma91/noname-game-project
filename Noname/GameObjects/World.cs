﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Noname.GameObjects
{
    public class World
    {
        public List<List<Entity>> WorldGrid;


        public World(Player player)
        {
            Player = player;
            FPSCounter = new FPSCounter("0", new Vector2(16, 16), Color.White);
        }


        public FPSCounter FPSCounter { get; private set; }
        public Player Player { get; private set; }
    }

}
