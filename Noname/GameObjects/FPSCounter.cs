﻿using System;
using Microsoft.Xna.Framework;


namespace Noname.GameObjects
{
    public class FPSCounter : TextEntity
    {
        public FPSCounter(string text, Vector2 position, Color color)
        {
            Text = text;
            Position = position;
            Color = color;
        }


        public String Text { get; set; }


        public int FrameCount { get; set; }


        public TimeSpan ElapsedTime { get; set; }


        public int FPS { get; set; }
    }
}

